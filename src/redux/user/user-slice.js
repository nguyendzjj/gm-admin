import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
    name: 'user',
    initialState: {

    },
    reducers: {
        login(){

        },
        loginSuccess(state, action){
            localStorage.setItem("MY_PROFILE", JSON.stringify(action.payload.data));
        }
    }
});

export const {
 login, loginSuccess
} = userSlice.actions;
const userReducer = userSlice.reducer;
export default userReducer;