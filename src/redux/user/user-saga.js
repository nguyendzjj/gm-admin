import { fork, put, take } from "redux-saga/effects";
import { login, loginSuccess } from "./user-slice";
import userAPI from "../../api/user-api";
import History from "../../utils/history";

async function fetchLogin(param) {
    // eslint-disable-next-line
    return await userAPI.login(param);
}

function* handleLogin(action) {
    try {
        const response = yield fetchLogin(action.payload);
        if (response && response.status === 200) {
            yield put(loginSuccess(response));
            History.push("/dashboard");
        }
    } catch (error) {
        console.log(error);
    }
}

function* watchLogin() {
    while (true) {
        const action = yield take(login.type);
        yield fork(handleLogin, action)
    }
}

export function* userSaga() {
    console.log('test');
    yield fork(watchLogin);
}