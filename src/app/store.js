import createSagaMiddleware from "@redux-saga/core";
import { configureStore } from '@reduxjs/toolkit'

import rootSaga from "./root-saga";
import userReducer from "../redux/user/user-slice";


const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
    reducer: {
        user: userReducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(sagaMiddleware)
});

sagaMiddleware.run(rootSaga);
