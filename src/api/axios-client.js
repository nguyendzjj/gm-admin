import axios from 'axios';

const baseURL = process.env.REACT_APP_API_ENDPOINT;
console.log(baseURL);

const axiosClient = axios.create({
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    },
    withCredentials: true,
    credentials: 'include'
});

// Add a request interceptor
axiosClient.interceptors.request.use(
    // eslint-disable-next-line
    function (req) {
        return req;
    },
    // eslint-disable-next-line
    function (error) {
        return Promise.reject(error);
    }
);


// Add a response interceptor
// eslint-disable-next-line
axiosClient.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    if (response && response.data) {
        const { status, data } = response
        return { status, data };

    }
    // eslint-disable-next-line
}, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    console.log(error);
    if (error && error.response && error.response.data && error.response.data.error === 'jwt must be provided') {
        localStorage.removeItem("MY_PROFILE");
        window.location.replace('/login');

    }
    return Promise.reject(error);
});

export { baseURL, axiosClient };
