import { baseURL, axiosClient } from './axios-client';

const userAPI = {
    login(param){
        const url = `${baseURL}/api/users/login`;
        return axiosClient.post(url, param);
    },
    getAllUser(){
        const url = `${baseURL}/api/admin/getAllUsers`;
        return axiosClient.get(url);
    }
}
export default userAPI;